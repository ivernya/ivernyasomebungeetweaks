package fr.keke142.ivernyasomebungeetweaks;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import de.themoep.randomteleport.RandomTeleport;
import de.themoep.randomteleport.api.RandomTeleportAPI;
import fr.keke142.ivernyasomebungeetweaks.configs.PlayerDataBridgeConfig;
import fr.keke142.ivernyasomebungeetweaks.listeners.*;
import fr.keke142.ivernyasomebungeetweaks.listeners.requests.EnderPortalsRequestListener;
import fr.keke142.ivernyasomebungeetweaks.listeners.requests.NetherPortalsRequestListener;
import fr.keke142.ivernyasomebungeetweaks.managers.*;
import fr.keke142.ivernyasomebungeetweaks.tasks.PlayerDataBridgeTask;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.*;
import java.util.Locale;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;

public class IvernyaSomeBungeeTweaksPlugin extends JavaPlugin {
    public static final int MAX_PLAYER_WAITING_MS = 50;

    private static IvernyaSomeBungeeTweaksPlugin instance;
    private RandomTeleportAPI randomTeleportAPI;

    private ConfigManager configManager;
    private DatabaseManager databaseManager;

    private NetherPortalsManager netherPortalsManager;
    private EnderPortalsManager enderPortalsManager;
    private PlayerDataBridgeManager playerDataBridgeManager;

    private ProtocolManager protocolManager;

    public static IvernyaSomeBungeeTweaksPlugin getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;

        protocolManager = ProtocolLibrary.getProtocolManager();

        configManager = new ConfigManager(this);
        databaseManager = new DatabaseManager(this);
        netherPortalsManager = new NetherPortalsManager(this);

        MessageManager.loadLocale(this, Locale.FRENCH);

        databaseManager.loadDatabase();

        if (getServer().getPluginManager().getPlugin("RandomTeleport") != null) {
            randomTeleportAPI = (RandomTeleport) getServer().getPluginManager().getPlugin("RandomTeleport");
        } else {

        }

        this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");

        PluginManager pluginManager = Bukkit.getPluginManager();

        pluginManager.registerEvents(new NetherPortalsListener(this), this);
        pluginManager.registerEvents(new NetherPortalsRequestListener(this), this);
        pluginManager.registerEvents(new BungeeTabCompletionListeners(this), this);


        enderPortalsManager = new EnderPortalsManager(this);
        pluginManager.registerEvents(new EnderPortalsListener(this), this);
        pluginManager.registerEvents(new EnderPortalsRequestListener(this), this);

        pluginManager.registerEvents(new EnderGatewayListener(this), this);

        PlayerDataBridgeConfig playerDataBridgeConfig = configManager.getPlayerDataBridgeConfig();
        playerDataBridgeManager = new PlayerDataBridgeManager(this);
        pluginManager.registerEvents(new PlayerDataBridgeListener(this), this);
        new PlayerDataBridgeTask(this).runTaskTimerAsynchronously(this, playerDataBridgeConfig.getSaveIntervalSecs() * 20, playerDataBridgeConfig.getSaveIntervalSecs() * 20);
    }

    @Override
    public void onDisable() {
        if (databaseManager.getHikari() != null) databaseManager.getHikari().close();
    }


    public ConfigManager getConfigManager() {
        return configManager;
    }

    public DatabaseManager getDatabaseManager() {
        return databaseManager;
    }

    public NetherPortalsManager getNetherPortalsManager() {
        return netherPortalsManager;
    }

    public RandomTeleportAPI getRandomTeleportAPI() {
        return randomTeleportAPI;
    }

    /**
     * Create a default configuration file from the .jar.
     *
     * @param actual      The destination file
     * @param defaultName The name of the file inside the jar's defaults folder
     */
    public void createDefaultConfiguration(File actual, String defaultName) {

        // Make parent directories
        File parent = actual.getParentFile();
        if (!parent.exists()) {
            parent.mkdirs();
        }

        if (actual.exists()) {
            return;
        }

        JarFile file = null;
        InputStream input = null;
        try {
            file = new JarFile(getFile());
            ZipEntry copy = file.getEntry(defaultName);
            if (copy == null) {
                file.close();
                throw new FileNotFoundException();
            }
            input = file.getInputStream(copy);
        } catch (IOException e) {
            getLogger().severe("Unable to read default configuration: " + defaultName);
        }

        if (input != null) {
            FileOutputStream output = null;

            try {
                output = new FileOutputStream(actual);
                byte[] buf = new byte[8192];
                int length = 0;
                while ((length = input.read(buf)) > 0) {
                    output.write(buf, 0, length);
                }

                getLogger().info("Default configuration file written: " + actual.getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    input.close();
                } catch (IOException ignore) {
                }

                try {
                    if (output != null) {
                        output.close();
                    }
                } catch (IOException ignore) {
                }
            }
        }
        if (file != null) {
            try {
                file.close();
            } catch (IOException ignore) {
            }
        }
    }

    public PlayerDataBridgeManager getPlayerDataBridgeManager() {
        return playerDataBridgeManager;
    }

    public EnderPortalsManager getEnderPortalsManager() {
        return enderPortalsManager;
    }

    public ProtocolManager getProtocolManager() {
        return protocolManager;
    }
}
