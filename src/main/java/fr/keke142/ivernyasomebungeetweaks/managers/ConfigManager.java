package fr.keke142.ivernyasomebungeetweaks.managers;

import fr.keke142.ivernyasomebungeetweaks.IvernyaSomeBungeeTweaksPlugin;
import fr.keke142.ivernyasomebungeetweaks.configs.*;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.File;
import java.io.IOException;

public class ConfigManager {
    private IvernyaSomeBungeeTweaksPlugin plugin;
    private DatabaseConfig database = new DatabaseConfig();
    private NetherPortalsConfig netherPortalsConfig = new NetherPortalsConfig();
    private EnderPortalsConfig enderPortalsConfig = new EnderPortalsConfig();
    private EnderGatewaysConfig enderGatewaysConfig = new EnderGatewaysConfig();
    private PlayerDataBridgeConfig playerDataBridgeConfig = new PlayerDataBridgeConfig();

    private File file;
    private FileConfiguration config;

    public ConfigManager(IvernyaSomeBungeeTweaksPlugin plugin) {
        this.plugin = plugin;
        file = new File(plugin.getDataFolder(), "config.yml");
        plugin.createDefaultConfiguration(file, "config.yml");

        config = plugin.getConfig();
        config.options().copyDefaults(true);
        plugin.saveDefaultConfig();

        database.load(config);
        netherPortalsConfig.load(config);
        enderPortalsConfig.load(config);
        enderGatewaysConfig.load(config);
        playerDataBridgeConfig.load(config);
    }

    public void save() {
        try {
            database.save(config);
            netherPortalsConfig.save(config);
            enderPortalsConfig.save(config);
            enderGatewaysConfig.save(config);
            playerDataBridgeConfig.save(config);
            config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void reload() {
        config = plugin.getConfig();
        config.options().copyDefaults(true);
        plugin.saveDefaultConfig();

        database.load(config);
        netherPortalsConfig.load(config);
        enderPortalsConfig.load(config);
        enderGatewaysConfig.load(config);
        playerDataBridgeConfig.load(config);
    }

    public DatabaseConfig getDatabaseConfig() {
        return database;
    }

    public NetherPortalsConfig getNetherPortalsConfig() {
        return netherPortalsConfig;
    }

    public EnderPortalsConfig getEnderPortalsConfig() {
        return enderPortalsConfig;
    }

    public EnderGatewaysConfig getEnderGatewaysConfig() {
        return enderGatewaysConfig;
    }

    public PlayerDataBridgeConfig getPlayerDataBridgeConfig() {
        return playerDataBridgeConfig;
    }
}
