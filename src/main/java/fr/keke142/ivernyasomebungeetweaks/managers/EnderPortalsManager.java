package fr.keke142.ivernyasomebungeetweaks.managers;

import fr.keke142.ivernyasomebungeetweaks.IvernyaSomeBungeeTweaksPlugin;
import net.minecraft.server.v1_15_R1.AdvancementProgress;
import net.minecraft.server.v1_15_R1.EntityPlayer;
import org.bukkit.NamespacedKey;
import org.bukkit.advancement.Advancement;
import org.bukkit.craftbukkit.v1_15_R1.advancement.CraftAdvancement;
import org.bukkit.craftbukkit.v1_15_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class EnderPortalsManager {
    private IvernyaSomeBungeeTweaksPlugin plugin;

    public EnderPortalsManager(IvernyaSomeBungeeTweaksPlugin plugin) {
        this.plugin = plugin;
    }

    public void grantEndAdvancements(Player player) {
        NamespacedKey enterTheEndKey = NamespacedKey.minecraft("story/enter_the_end");
        NamespacedKey endRootKey = NamespacedKey.minecraft("end/root");

        Advancement enterTheEndAdvancement = plugin.getServer().getAdvancement(enterTheEndKey);
        net.minecraft.server.v1_15_R1.Advancement enterTheEndAdvancementNMS = ((CraftAdvancement) enterTheEndAdvancement).getHandle();

        Advancement endRootAdvancement = plugin.getServer().getAdvancement(endRootKey);
        net.minecraft.server.v1_15_R1.Advancement endRootAdvancementNMS = ((CraftAdvancement) endRootAdvancement).getHandle();

        EntityPlayer entP = ((CraftPlayer) player).getHandle();

        AdvancementProgress enterTheENdAdvancementProgress = entP.getAdvancementData().getProgress(enterTheEndAdvancementNMS);

        if (!enterTheENdAdvancementProgress.isDone()) {
            for (String s : enterTheENdAdvancementProgress.getRemainingCriteria()) {
                entP.getAdvancementData().grantCriteria(enterTheEndAdvancementNMS, s);
            }
        }

        AdvancementProgress endRootAdvancementProgress = entP.getAdvancementData().getProgress(endRootAdvancementNMS);

        if (!endRootAdvancementProgress.isDone()) {
            for (String s : endRootAdvancementProgress.getRemainingCriteria()) {
                entP.getAdvancementData().grantCriteria(endRootAdvancementNMS, s);
            }
        }
    }
}
