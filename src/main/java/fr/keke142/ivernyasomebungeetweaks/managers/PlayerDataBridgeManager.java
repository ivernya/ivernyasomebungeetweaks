package fr.keke142.ivernyasomebungeetweaks.managers;

import com.gmail.tracebachi.DeltaRedis.Shared.Servers;
import com.gmail.tracebachi.DeltaRedis.Spigot.DeltaRedisApi;
import fr.keke142.ivernyasomebungeetweaks.IvernyaSomeBungeeTweaksPlugin;
import net.minecraft.server.v1_15_R1.EntityPlayer;
import net.minecraft.server.v1_15_R1.MinecraftServer;
import net.minecraft.server.v1_15_R1.ServerStatisticManager;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_15_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Properties;
import java.util.UUID;

public class PlayerDataBridgeManager {
    public static final int MAX_DATA_UPDATE_WAITING_MS = 5;
    private IvernyaSomeBungeeTweaksPlugin plugin;

    public PlayerDataBridgeManager(IvernyaSomeBungeeTweaksPlugin plugin) {
        this.plugin = plugin;
    }

    private void savePlayerStats(Player player) throws Exception {
        Object server = MinecraftServer.class.getMethod("getServer").invoke(null);
        Object playerList = server.getClass().getMethod("getPlayerList").invoke(server);
        Object entityPlayer = player.getClass().getMethod("getHandle").invoke(player);

        Method savePlayerFileMethod = playerList.getClass().getSuperclass().getDeclaredMethod("savePlayerFile", EntityPlayer.class);

        savePlayerFileMethod.setAccessible(true);
        savePlayerFileMethod.invoke(playerList, entityPlayer);
        savePlayerFileMethod.setAccessible(false);
    }

    public void bungeeLoadDataForPlayer(String playerUuid, String jsonAdvancements, String jsonStats, boolean useRefresh) {
        try {
            BufferedReader is = new BufferedReader(new FileReader("server.properties"));
            Properties props = new Properties();
            props.load(is);
            is.close();
            String levelName = props.getProperty("level-name");

            String worldPath = plugin.getServer().getWorldContainer().getAbsolutePath();

            File advancementFolder = new File(worldPath + "/" + levelName + "/advancements");
            File statsFolder = new File(worldPath + "/" + levelName + "/stats");

            if (!advancementFolder.exists()) advancementFolder.mkdir();
            if (!statsFolder.exists()) statsFolder.mkdir();

            File playerAdvancementsFile = new File(advancementFolder.getAbsolutePath() + "/" + playerUuid + ".json");
            File playerStatsFile = new File(statsFolder.getAbsolutePath() + "/" + playerUuid + ".json");

            if (!playerAdvancementsFile.exists()) playerAdvancementsFile.createNewFile();
            if (!playerStatsFile.exists()) playerStatsFile.createNewFile();

            try (FileWriter advancementsFile = new FileWriter(playerAdvancementsFile, false)) {
                advancementsFile.write(jsonAdvancements);
                advancementsFile.flush();

                try (FileWriter statsFile = new FileWriter(playerStatsFile, false)) {
                    statsFile.write(jsonStats);
                    statsFile.flush();

                    if (useRefresh) {
                        long startTime = System.currentTimeMillis();

                        while ((System.currentTimeMillis() - startTime) < MAX_DATA_UPDATE_WAITING_MS) {
                            if (Bukkit.getPlayer(UUID.fromString(playerUuid)) == null) continue;

                            long elapsedWaitingTime = (System.currentTimeMillis() - startTime);

                            Player player = Bukkit.getPlayer(UUID.fromString(playerUuid));

                            plugin.getLogger().info("Waiting " + player.getName() + " for advancements and stats took " + elapsedWaitingTime + " ms");


                            EntityPlayer entP = ((CraftPlayer) player).getHandle();

                            //For advancements
                            entP.getAdvancementData().b();

                            //For stats
                            Field dField = ServerStatisticManager.class.getDeclaredField("d");

                            dField.setAccessible(true);
                            File dResolved = (File) dField.get(entP.getStatisticManager());

                            entP.getStatisticManager().a(MinecraftServer.getServer().aC(), org.apache.commons.io.FileUtils.readFileToString(dResolved));
                            dField.setAccessible(false);

                            long elapsedTotalTime = (System.currentTimeMillis() - startTime);

                            plugin.getLogger().info("Loaded stats and advancements of " + player.getName() + " in " + elapsedTotalTime + " ms");
                            break;
                        }
                    }
                } catch (IOException | NoSuchFieldException | IllegalAccessException ex) {
                    ex.printStackTrace();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void bungeeSaveDataForPlayer(Player p, boolean triggerRefresh) {
        DeltaRedisApi deltaApi = DeltaRedisApi.instance();
        try {
            savePlayerStats(p);

            try {

                BufferedReader is = new BufferedReader(new FileReader("server.properties"));
                Properties props = new Properties();
                props.load(is);
                is.close();
                String levelName = props.getProperty("level-name");

                String playerUuid = p.getUniqueId().toString();

                String worldPath = plugin.getServer().getWorldContainer().getAbsolutePath();

                File advancementFolder = new File(worldPath + "/" + levelName + "/advancements");
                File statsFolder = new File(worldPath + "/" + levelName + "/stats");

                File playerAdvancementsFile = new File(advancementFolder.getAbsolutePath() + "/" + playerUuid + ".json");
                File playerStatsFile = new File(statsFolder.getAbsolutePath() + "/" + playerUuid + ".json");

                JSONParser jsonParser = new JSONParser();

                try (FileReader advancementsReader = new FileReader(playerAdvancementsFile)) {
                    Object parsedAdvancements = jsonParser.parse(advancementsReader);

                    JSONObject jsonAdvancements = (JSONObject) parsedAdvancements;


                    try (FileReader statsReader = new FileReader(playerStatsFile)) {
                        Object parsedStats = jsonParser.parse(statsReader);

                        JSONObject jsonStats = (JSONObject) parsedStats;

                        deltaApi.publish(Servers.SPIGOT, "sendPlayerDataToServers", playerUuid, jsonAdvancements.toJSONString(), jsonStats.toJSONString(), String.valueOf(triggerRefresh));
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                } catch (IOException | ParseException ex) {
                    ex.printStackTrace();
                }

            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
