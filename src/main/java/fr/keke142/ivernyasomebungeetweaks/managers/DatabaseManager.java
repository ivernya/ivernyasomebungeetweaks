package fr.keke142.ivernyasomebungeetweaks.managers;

import com.zaxxer.hikari.HikariDataSource;
import fr.keke142.ivernyasomebungeetweaks.IvernyaSomeBungeeTweaksPlugin;
import fr.keke142.ivernyasomebungeetweaks.database.NetherPortals;

public class DatabaseManager {
    private IvernyaSomeBungeeTweaksPlugin plugin;

    private NetherPortals netherPortals;

    private HikariDataSource hikari;

    public DatabaseManager(IvernyaSomeBungeeTweaksPlugin plugin) {
        this.plugin = plugin;
    }

    public void loadDatabase() {
        hikari = new HikariDataSource();
        hikari.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
        hikari.addDataSourceProperty("serverName", plugin.getConfigManager().getDatabaseConfig().getHost());
        hikari.addDataSourceProperty("port", plugin.getConfigManager().getDatabaseConfig().getPort());
        hikari.addDataSourceProperty("databaseName", plugin.getConfigManager().getDatabaseConfig().getDatabase());
        hikari.addDataSourceProperty("user", plugin.getConfigManager().getDatabaseConfig().getUserName());
        hikari.addDataSourceProperty("password", plugin.getConfigManager().getDatabaseConfig().getPassword());

        netherPortals = new NetherPortals(this);
        netherPortals.createTable();
    }

    public HikariDataSource getHikari() {
        return hikari;
    }

    public NetherPortals getNetherPortals() {
        return netherPortals;
    }
}
