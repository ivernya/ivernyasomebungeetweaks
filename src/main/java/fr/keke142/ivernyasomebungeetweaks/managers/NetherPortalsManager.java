package fr.keke142.ivernyasomebungeetweaks.managers;

import fr.keke142.ivernyasomebungeetweaks.IvernyaSomeBungeeTweaksPlugin;
import fr.keke142.ivernyasomebungeetweaks.objects.NetherPortal;
import net.minecraft.server.v1_15_R1.AdvancementProgress;
import net.minecraft.server.v1_15_R1.EntityPlayer;
import org.bukkit.NamespacedKey;
import org.bukkit.advancement.Advancement;
import org.bukkit.craftbukkit.v1_15_R1.advancement.CraftAdvancement;
import org.bukkit.craftbukkit.v1_15_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class NetherPortalsManager {
    private DatabaseManager databaseManager;
    private IvernyaSomeBungeeTweaksPlugin plugin;

    public NetherPortalsManager(IvernyaSomeBungeeTweaksPlugin plugin) {
        this.plugin = plugin;
        databaseManager = plugin.getDatabaseManager();
    }

    public void savePortalAndDestination(String fromServer, String fromWorld, double fromX, double fromY, double fromZ, String toWorld, double toX, double toY, double toZ) {
        databaseManager.getNetherPortals().savePortalAndDestination(fromServer, fromWorld, fromX, fromY, fromZ, toWorld, toX, toY, toZ);
    }

    public void grantNetherAdvancements(Player player) {
        NamespacedKey enterTheNetherKey = NamespacedKey.minecraft("story/enter_the_nether");
        NamespacedKey netherRootKey = NamespacedKey.minecraft("nether/root");

        Advancement enterTheNetherAdvancement = plugin.getServer().getAdvancement(enterTheNetherKey);
        net.minecraft.server.v1_15_R1.Advancement enterTheNetherAdvancementNMS = ((CraftAdvancement) enterTheNetherAdvancement).getHandle();

        Advancement netherRootAdvancement = plugin.getServer().getAdvancement(netherRootKey);
        net.minecraft.server.v1_15_R1.Advancement netherRootAdvancementNMS = ((CraftAdvancement) netherRootAdvancement).getHandle();

        EntityPlayer entP = ((CraftPlayer) player).getHandle();

        AdvancementProgress enterTheNetherAdvancementProgress = entP.getAdvancementData().getProgress(enterTheNetherAdvancementNMS);

        if (!enterTheNetherAdvancementProgress.isDone()) {
            for (String s : enterTheNetherAdvancementProgress.getRemainingCriteria()) {
                entP.getAdvancementData().grantCriteria(enterTheNetherAdvancementNMS, s);
            }
        }

        AdvancementProgress netherRootAdvancementProgress = entP.getAdvancementData().getProgress(netherRootAdvancementNMS);

        if (!netherRootAdvancementProgress.isDone()) {
            for (String s : netherRootAdvancementProgress.getRemainingCriteria()) {
                entP.getAdvancementData().grantCriteria(netherRootAdvancementNMS, s);
            }
        }
    }

    public NetherPortal getPortalToFromFrom(String fromServer, String fromWorld, double fromX, double fromY, double fromZ) {
        return databaseManager.getNetherPortals().getPortalToFromFrom(fromServer, fromWorld, fromX, fromY, fromZ);
    }
}
