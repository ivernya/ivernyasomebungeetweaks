package fr.keke142.ivernyasomebungeetweaks.tasks;

import fr.keke142.ivernyasomebungeetweaks.IvernyaSomeBungeeTweaksPlugin;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class PlayerDataBridgeTask extends BukkitRunnable {
    private IvernyaSomeBungeeTweaksPlugin plugin;

    public PlayerDataBridgeTask(IvernyaSomeBungeeTweaksPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public void run() {
        for (Player p : Bukkit.getOnlinePlayers()) {
            plugin.getPlayerDataBridgeManager().bungeeSaveDataForPlayer(p, false);
        }
    }
}
