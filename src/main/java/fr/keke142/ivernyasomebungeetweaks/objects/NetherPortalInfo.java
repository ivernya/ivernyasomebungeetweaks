package fr.keke142.ivernyasomebungeetweaks.objects;

import org.bukkit.Location;

public class NetherPortalInfo {
    private final Axis axis;
    private final Location center;

    public NetherPortalInfo(Axis axis, Location center) {
        this.axis = axis;
        this.center = center;
    }

    public Axis getAxis() {
        return axis;
    }

    public Location getCenter() {
        return center;
    }

    public enum Axis {
        x, z;
    }
}
