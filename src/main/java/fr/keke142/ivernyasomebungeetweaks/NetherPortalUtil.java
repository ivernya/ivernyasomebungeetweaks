package fr.keke142.ivernyasomebungeetweaks;

import fr.keke142.ivernyasomebungeetweaks.objects.NetherPortalInfo;
import org.bukkit.Axis;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.data.Orientable;

public class NetherPortalUtil {
    private static final Material[][][] PORTAL_TEMPLATE = new Material[4][3][5];

    static {
        // Base
        PORTAL_TEMPLATE[0][1][0] = Material.OBSIDIAN;
        PORTAL_TEMPLATE[1][1][0] = Material.OBSIDIAN;
        PORTAL_TEMPLATE[2][1][0] = Material.OBSIDIAN;
        PORTAL_TEMPLATE[3][1][0] = Material.OBSIDIAN;

        // Frame
        PORTAL_TEMPLATE[0][1][1] = Material.OBSIDIAN;
        PORTAL_TEMPLATE[0][1][2] = Material.OBSIDIAN;
        PORTAL_TEMPLATE[0][1][3] = Material.OBSIDIAN;
        PORTAL_TEMPLATE[3][1][1] = Material.OBSIDIAN;
        PORTAL_TEMPLATE[3][1][2] = Material.OBSIDIAN;
        PORTAL_TEMPLATE[3][1][3] = Material.OBSIDIAN;

        // Top
        PORTAL_TEMPLATE[0][1][4] = Material.OBSIDIAN;
        PORTAL_TEMPLATE[1][1][4] = Material.OBSIDIAN;
        PORTAL_TEMPLATE[2][1][4] = Material.OBSIDIAN;
        PORTAL_TEMPLATE[3][1][4] = Material.OBSIDIAN;

        // Portal
        PORTAL_TEMPLATE[1][1][1] = Material.NETHER_PORTAL;
        PORTAL_TEMPLATE[1][1][2] = Material.NETHER_PORTAL;
        PORTAL_TEMPLATE[1][1][3] = Material.NETHER_PORTAL;
        PORTAL_TEMPLATE[2][1][1] = Material.NETHER_PORTAL;
        PORTAL_TEMPLATE[2][1][2] = Material.NETHER_PORTAL;
        PORTAL_TEMPLATE[2][1][3] = Material.NETHER_PORTAL;

        // Ledges
        PORTAL_TEMPLATE[0][0][0] = Material.OBSIDIAN;
        PORTAL_TEMPLATE[1][0][0] = Material.OBSIDIAN;
        PORTAL_TEMPLATE[2][0][0] = Material.OBSIDIAN;
        PORTAL_TEMPLATE[3][0][0] = Material.OBSIDIAN;
        PORTAL_TEMPLATE[0][2][0] = Material.OBSIDIAN;
        PORTAL_TEMPLATE[1][2][0] = Material.OBSIDIAN;
        PORTAL_TEMPLATE[2][2][0] = Material.OBSIDIAN;
        PORTAL_TEMPLATE[3][2][0] = Material.OBSIDIAN;

        // Roof
        PORTAL_TEMPLATE[0][0][4] = Material.COBBLESTONE;
        PORTAL_TEMPLATE[1][0][4] = Material.COBBLESTONE;
        PORTAL_TEMPLATE[2][0][4] = Material.COBBLESTONE;
        PORTAL_TEMPLATE[3][0][4] = Material.COBBLESTONE;
        PORTAL_TEMPLATE[0][2][4] = Material.COBBLESTONE;
        PORTAL_TEMPLATE[1][2][4] = Material.COBBLESTONE;
        PORTAL_TEMPLATE[2][2][4] = Material.COBBLESTONE;
        PORTAL_TEMPLATE[3][2][4] = Material.COBBLESTONE;
    }

    public static void createPortal(Location location, NetherPortalInfo.Axis axis) {
        int xFacing = axis == NetherPortalInfo.Axis.x ? 1 : 0;
        int zFacing = axis == NetherPortalInfo.Axis.z ? 1 : 0;
        for (int length = 0; length < PORTAL_TEMPLATE.length; length++) {
            for (int depth = 0; depth < PORTAL_TEMPLATE[0].length; depth++) {
                for (int height = 0; height < PORTAL_TEMPLATE[0][0].length; height++) {
                    Material material = PORTAL_TEMPLATE[length][depth][height];
                    if (material == null) {
                        material = Material.AIR;
                    }
                    int xOffset = (xFacing * (length - 1)) + (zFacing * (depth - 1));
                    int yOffset = height - 1;
                    int zOffset = (zFacing * (length - 1)) + (xFacing * (depth - 1));
                    Block block = location.getBlock().getRelative(xOffset, yOffset, zOffset);
                    block.setType(material, false);
                    if (material == Material.NETHER_PORTAL) {

                        Orientable orientable = (Orientable) block.getBlockData();

                        if (axis == NetherPortalInfo.Axis.z) {
                            orientable.setAxis(Axis.Z);
                        } else {
                            orientable.setAxis(Axis.X);
                        }

                        block.setBlockData(orientable);
                    }
                }
            }
        }
    }

    public static NetherPortalInfo getPortalInfo(Block portal) throws IllegalArgumentException {
        if (portal.getType() != Material.NETHER_PORTAL) {
            return null;
        }

        double minX = 0;
        double maxX = 0;
        double minY = 0;
        //double maxY = 0;
        double minZ = 0;
        double maxZ = 0;
        while (portal.getRelative((int) minX - 1, 0, 0).getType() == Material.NETHER_PORTAL) {
            minX--;
        }
        if (portal.getRelative((int) minX - 1, 0, 0).getType() == Material.OBSIDIAN) {
            while (portal.getRelative((int) maxX + 1, 0, 0).getType() == Material.NETHER_PORTAL) {
                maxX++;
            }
            if (portal.getRelative((int) minX - 1, 0, 0).getType() != Material.OBSIDIAN) {
                minX = 0;
                maxX = 0;
            }
        } else {
            minX = 0;
            maxX = 0;
        }
        while (portal.getRelative(0, (int) minY - 1, 0).getType() == Material.NETHER_PORTAL) {
            minY--;
        }
        if (portal.getRelative(0, (int) minY - 1, 0).getType() != Material.OBSIDIAN) {
            minY = 0;
        }
        while (portal.getRelative(0, 0, (int) minZ - 1).getType() == Material.NETHER_PORTAL) {
            minZ--;
        }
        if (portal.getRelative(0, 0, (int) minZ - 1).getType() == Material.OBSIDIAN) {
            while (portal.getRelative(0, 0, (int) maxZ + 1).getType() == Material.NETHER_PORTAL) {
                maxZ++;
            }
            if (portal.getRelative(0, 0, (int) minZ - 1).getType() != Material.OBSIDIAN) {
                minZ = 0;
                maxZ = 0;
            }
        } else {
            minZ = 0;
            maxZ = 0;
        }
        double x = portal.getX() + ((maxX + 1.0) + minX) / 2.0;
        double y = portal.getY() + minY;
        double z = portal.getZ() + ((maxZ + 1.0) + minZ) / 2.0;
        Location loc = new Location(portal.getWorld(), x, y, z);
        NetherPortalInfo.Axis axis;
        if (maxZ == 0 && minZ == 0) {
            axis = NetherPortalInfo.Axis.x;
        } else {
            axis = NetherPortalInfo.Axis.z;
        }
        return new NetherPortalInfo(axis, loc);
    }
}
