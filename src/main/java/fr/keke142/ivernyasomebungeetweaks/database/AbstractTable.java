package fr.keke142.ivernyasomebungeetweaks.database;

import fr.keke142.ivernyasomebungeetweaks.IvernyaSomeBungeeTweaksPlugin;
import fr.keke142.ivernyasomebungeetweaks.managers.DatabaseManager;

import java.sql.SQLException;
import java.util.logging.Level;

public abstract class AbstractTable {
    private DatabaseManager databaseManager;

    public AbstractTable(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
    }

    public abstract String[] getTable();

    public void createTable() {
        String[] tableInformation = getTable();

        String createTableQuery = "CREATE TABLE IF NOT EXISTS `" + tableInformation[0] + "` (" + tableInformation[1] + ") " + tableInformation[2] + ";";

        try {
            databaseManager.getHikari().getConnection().prepareStatement(createTableQuery);
        } catch (SQLException e) {
            IvernyaSomeBungeeTweaksPlugin.getInstance().getLogger().log(Level.SEVERE, "Failed to create a table", e);
            e.printStackTrace();
        }
    }

    public DatabaseManager getDatabaseManager() {
        return databaseManager;
    }
}
