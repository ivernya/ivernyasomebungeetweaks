package fr.keke142.ivernyasomebungeetweaks.database;

import fr.keke142.ivernyasomebungeetweaks.managers.DatabaseManager;
import fr.keke142.ivernyasomebungeetweaks.objects.NetherPortal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class NetherPortals extends AbstractTable {
    private static final String GET_PORTAL_FROM_FROM_TO = "SELECT portalFromServer, portalFromWorld, portalFromX, portalFromY, portalFromZ FROM ivbt_netherportals WHERE portalToWorld=? AND portalToX=? AND portalToY=? AND portalToZ=?";
    private static final String GET_PORTAL_TO_FROM_FROM = "SELECT portalToWorld, portalToX, portalToY, portalToZ FROM ivbt_netherportals WHERE portalFromServer=? AND portalFromWorld=? AND portalFromX=? AND portalFromY=? AND portalFromZ=?";
    private static final String SAVE_PORTAL_AND_DESTINATION = "INSERT INTO ivbt_netherportals (portalFromServer, portalFromWorld, portalFromX, portalFromY, portalFromZ, portalToWorld, portalToX, portalToY, portalToZ) VALUES(?,?,?,?,?,?,?,?,?)";

    public NetherPortals(DatabaseManager databaseManager) {
        super(databaseManager);
    }

    public void savePortalAndDestination(String fromServer, String fromWorld, double fromX, double fromY, double fromZ, String toWorld, double toX, double toY, double toZ) {
        try (Connection connection = getDatabaseManager().getHikari().getConnection();
             PreparedStatement savePortalAndDestination = connection.prepareStatement(SAVE_PORTAL_AND_DESTINATION)) {
            savePortalAndDestination.setString(1, fromServer);
            savePortalAndDestination.setString(2, fromWorld);
            savePortalAndDestination.setDouble(3, fromX);
            savePortalAndDestination.setDouble(4, fromY);
            savePortalAndDestination.setDouble(5, fromZ);
            savePortalAndDestination.setString(6, toWorld);
            savePortalAndDestination.setDouble(7, toX);
            savePortalAndDestination.setDouble(8, toY);
            savePortalAndDestination.setDouble(9, toZ);

            savePortalAndDestination.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public NetherPortal getPortalFromFromTo(String toWorld, double toX, double toY, double toZ) {
        try (Connection connection = getDatabaseManager().getHikari().getConnection();
             PreparedStatement getPortalFromFromTo = connection.prepareStatement(GET_PORTAL_FROM_FROM_TO)) {
            getPortalFromFromTo.setString(1, toWorld);
            getPortalFromFromTo.setDouble(2, toX);
            getPortalFromFromTo.setDouble(3, toY);
            getPortalFromFromTo.setDouble(4, toZ);

            ResultSet res = getPortalFromFromTo.executeQuery();
            while (res.next()) {
                String server = res.getString("portalFromServer");
                String world = res.getString("portalFromWorld");
                double x = res.getDouble("portalFromX");
                double y = res.getDouble("portalFromY");
                double z = res.getDouble("portalFromZ");

                return new NetherPortal(server, world, x, y, z);
            }
            res.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public NetherPortal getPortalToFromFrom(String fromServer, String fromWorld, double fromX, double fromY, double fromZ) {
        try (Connection connection = getDatabaseManager().getHikari().getConnection();
             PreparedStatement getPortalToFromFrom = connection.prepareStatement(GET_PORTAL_TO_FROM_FROM)) {
            getPortalToFromFrom.setString(1, fromServer);
            getPortalToFromFrom.setString(2, fromWorld);
            getPortalToFromFrom.setDouble(3, fromX);
            getPortalToFromFrom.setDouble(4, fromY);
            getPortalToFromFrom.setDouble(5, fromZ);

            ResultSet res = getPortalToFromFrom.executeQuery();
            while (res.next()) {
                String world = res.getString("portalToWorld");
                double x = res.getDouble("portalToX");
                double y = res.getDouble("portalToY");
                double z = res.getDouble("portalToZ");

                return new NetherPortal(world, x, y, z);
            }
            res.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public String[] getTable() {
        return new String[]{"ivbt_netherportals", "`portalFromServer` VARCHAR(36) NOT NULL, " +
                "`portalFromWorld` VARCHAR(36) NOT NULL, " +
                "`portalFromX` DOUBLE NOT NULL," +
                "`portalFromY` DOUBLE NOT NULL, " +
                "`portalFromZ` DOUBLE NOT NULL, " +
                "`portalToWorld` VARCHAR(36) NOT NULL, " +
                "`portalToX` DOUBLE NOT NULL, " +
                "`portalToY` DOUBLE NOT NULL, " +
                "`portalToZ` DOUBLE NOT NULL, " +
                "PRIMARY KEY (`portalFromServer`, `portalFromWorld`, `portalFromX`, `portalFromY`, `portalFromZ`)",
                "ENGINE=MyISAM DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci"};
    }
}
