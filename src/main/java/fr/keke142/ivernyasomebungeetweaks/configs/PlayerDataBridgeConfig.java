package fr.keke142.ivernyasomebungeetweaks.configs;

import org.bukkit.configuration.Configuration;

public class PlayerDataBridgeConfig {
    private int saveIntervalSecs;

    public void load(Configuration config) {
        saveIntervalSecs = config.getInt("playerDataBridge.saveIntervalSecs", 300);
    }

    public void save(Configuration config) {
        config.set("playerDataBridge.saveIntervalSecs", saveIntervalSecs);
    }

    public int getSaveIntervalSecs() {
        return saveIntervalSecs;
    }
}
