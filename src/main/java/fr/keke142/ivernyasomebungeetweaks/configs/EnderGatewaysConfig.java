package fr.keke142.ivernyasomebungeetweaks.configs;

import org.bukkit.configuration.Configuration;

public class EnderGatewaysConfig {
    private int gatewaysMinRange;
    private int gatewaysMaxRange;

    public void load(Configuration config) {
        gatewaysMinRange = config.getInt("enderGateways.enderMinRange", 5000);
        gatewaysMaxRange = config.getInt("enderGatways.enderMaxRange", 5000000);
    }

    public void save(Configuration config) {
        config.set("enderGateways.gatewaysMinRange", gatewaysMinRange);
        config.set("enderGateways.gatewaysMaxRange", gatewaysMaxRange);
    }

    public int getGatewaysMinRange() {
        return gatewaysMinRange;
    }

    public int getGatewaysMaxRange() {
        return gatewaysMaxRange;
    }
}
