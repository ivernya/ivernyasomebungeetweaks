package fr.keke142.ivernyasomebungeetweaks.configs;

import org.bukkit.configuration.Configuration;

public class EnderPortalsConfig {
    private String enderServer;
    private String enderWorld;

    public void load(Configuration config) {
        enderServer = config.getString("enderPortals.enderServer", "server");
        enderWorld = config.getString("enderPortals.enderWorld", "world_the_end");
    }

    public void save(Configuration config) {
        config.set("enderPortals.enderServer", enderServer);
        config.set("enderPortals.enderWorld", enderWorld);
    }

    public String getEnderServer() {
        return enderServer;
    }

    public String getEnderWorld() {
        return enderWorld;
    }
}
