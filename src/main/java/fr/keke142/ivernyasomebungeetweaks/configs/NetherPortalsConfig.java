package fr.keke142.ivernyasomebungeetweaks.configs;

import org.bukkit.configuration.Configuration;

public class NetherPortalsConfig {
    private String netherServer;
    private String netherWorld;
    private int netherMinRange;
    private int netherMaxRange;
    private int netherMinY;
    private int netherMaxY;

    public void load(Configuration config) {
        netherServer = config.getString("netherPortals.netherServer", "nether");
        netherWorld = config.getString("netherPortals.netherWorld", "world_nether");
        netherMinRange = config.getInt("netherPortals.netherMinRange", 0);
        netherMaxRange = config.getInt("netherPortals.netherMaxRange", 1000000);
        netherMinY = config.getInt("netherPortals.netherMinY", 70);
        netherMaxY = config.getInt("netherPortals.netherMaxY", 118);
    }

    public void save(Configuration config) {
        config.set("netherPortals.netherServer", netherServer);
        config.set("netherPortals.netherWorld", netherWorld);
        config.set("netherPortals.netherMinRange", netherMinRange);
        config.set("netherPortals.netherMaxRange", netherMaxRange);
        config.set("netherPortals.netherMinY", netherMinY);
        config.set("netherPortals.netherMaxY", netherMaxY);
    }

    public String getNetherServer() {
        return netherServer;
    }

    public String getNetherWorld() {
        return netherWorld;
    }

    public int getNetherMinRange() {
        return netherMinRange;
    }

    public int getNetherMaxRange() {
        return netherMaxRange;
    }

    public int getNetherMinY() {
        return netherMinY;
    }

    public int getNetherMaxY() {
        return netherMaxY;
    }
}
