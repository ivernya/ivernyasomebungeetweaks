package fr.keke142.ivernyasomebungeetweaks.listeners.requests;

import com.gmail.tracebachi.DeltaRedis.Spigot.DeltaRedisMessageEvent;
import de.themoep.randomteleport.searcher.RandomSearcher;
import fr.keke142.ivernyasomebungeetweaks.IvernyaSomeBungeeTweaksPlugin;
import fr.keke142.ivernyasomebungeetweaks.NetherPortalUtil;
import fr.keke142.ivernyasomebungeetweaks.configs.NetherPortalsConfig;
import fr.keke142.ivernyasomebungeetweaks.managers.MessageManager;
import fr.keke142.ivernyasomebungeetweaks.objects.NetherPortal;
import fr.keke142.ivernyasomebungeetweaks.objects.NetherPortalInfo;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.UUID;

import static com.gmail.tracebachi.DeltaRedis.Shared.SplitPatterns.DELTA;
import static fr.keke142.ivernyasomebungeetweaks.IvernyaSomeBungeeTweaksPlugin.MAX_PLAYER_WAITING_MS;

public class NetherPortalsRequestListener implements Listener {
    private IvernyaSomeBungeeTweaksPlugin plugin;

    public NetherPortalsRequestListener(IvernyaSomeBungeeTweaksPlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onNetherPortalsRequest(DeltaRedisMessageEvent event) {
        String channel = event.getChannel();
        String eventMessage = event.getMessage();

        String[] splitMessage = DELTA.split(eventMessage);

        if (channel.equals("teleportToOverworldPortalRequest")) {
            String toPortalWorldName = splitMessage[0];

            double toPortalX = Double.parseDouble(splitMessage[1]);
            double toPortalY = Double.parseDouble(splitMessage[2]);
            double toPortalZ = Double.parseDouble(splitMessage[3]);
            String playerUUID = splitMessage[4];
            float playerYaw = Float.parseFloat(splitMessage[5]);
            float playerPitch = Float.parseFloat(splitMessage[6]);

            long startTime = System.currentTimeMillis();

            new BukkitRunnable() {
                @Override
                public void run() {
                    long elapsedTime = (System.currentTimeMillis() - startTime);

                    if (elapsedTime < MAX_PLAYER_WAITING_MS) {
                        this.cancel();
                    }

                    if (Bukkit.getPlayer(UUID.fromString(playerUUID)) != null) {
                        Player player = Bukkit.getPlayer(UUID.fromString(playerUUID));

                        World toPortalWorld = Bukkit.getWorld(toPortalWorldName);
                        if (toPortalWorld == null) {
                            player.sendMessage(MessageManager.msg("unknownDestinationWorld"));
                            this.cancel();
                            return;
                        }

                        Location portalLocation = new Location(toPortalWorld, toPortalX, toPortalY, toPortalZ, playerYaw, playerPitch);

                        player.teleport(portalLocation);
                        this.cancel();
                    }
                }
            }.runTaskTimer(plugin, 1, 1);
        }

        if (channel.equals("netherPortalsRequest")) {

            String fromPortalServer = splitMessage[0];
            String fromPortalWorldName = splitMessage[1];

            double fromPortalX = Double.parseDouble(splitMessage[2]);
            double fromPortalY = Double.parseDouble(splitMessage[3]);
            double fromPortalZ = Double.parseDouble(splitMessage[4]);
            String playerUUID = splitMessage[5];
            boolean portalAxis = Boolean.parseBoolean(splitMessage[6]);
            float playerYaw = Float.parseFloat(splitMessage[7]);
            float playerPitch = Float.parseFloat(splitMessage[8]);

            NetherPortalInfo.Axis axis;

            if (portalAxis) {
                axis = NetherPortalInfo.Axis.x;
            } else {
                axis = NetherPortalInfo.Axis.z;
            }

            NetherPortal portal = plugin.getNetherPortalsManager().getPortalToFromFrom(fromPortalServer, fromPortalWorldName, fromPortalX, fromPortalY, fromPortalZ);
            if (portal != null) {
                String toPortalWorldName = portal.getWorld();
                double toPortalX = portal.getX();
                double toPortalY = portal.getY();
                double toPortalZ = portal.getZ();

                long startTime = System.currentTimeMillis();

                new BukkitRunnable() {
                    @Override
                    public void run() {
                        long elapsedTime = (System.currentTimeMillis() - startTime);

                        if (elapsedTime < MAX_PLAYER_WAITING_MS) {
                            this.cancel();
                        }

                        if (Bukkit.getPlayer(UUID.fromString(playerUUID)) != null) {
                            Player player = Bukkit.getPlayer(UUID.fromString(playerUUID));

                            World toPortalWorld = Bukkit.getWorld(toPortalWorldName);
                            if (toPortalWorld == null) {
                                player.sendMessage(MessageManager.msg("unknownDestinationWorld"));
                                this.cancel();
                                return;
                            }

                            Location portalLocation = new Location(toPortalWorld, toPortalX, toPortalY, toPortalZ, playerYaw, playerPitch);

                            player.teleport(portalLocation);
                            plugin.getNetherPortalsManager().grantNetherAdvancements(player);

                            if (axis == NetherPortalInfo.Axis.x) {
                                portalLocation.subtract(1, 0, 0);
                            } else {
                                portalLocation.subtract(0, 0, 1);
                            }
                            portalLocation.getChunk().load();
                            NetherPortalUtil.createPortal(portalLocation, axis);

                            this.cancel();
                        }
                    }
                }.runTaskTimer(plugin, 1, 1);
            } else {

                long startTime = System.currentTimeMillis();

                new BukkitRunnable() {
                    @Override
                    public void run() {
                        long elapsedTime = (System.currentTimeMillis() - startTime);

                        if (elapsedTime < MAX_PLAYER_WAITING_MS) {
                            this.cancel();
                        }

                        if (Bukkit.getPlayer(UUID.fromString(playerUUID)) != null) {
                            Player player = Bukkit.getPlayer(UUID.fromString(playerUUID));

                            NetherPortalsConfig netherPortalsConfig = plugin.getConfigManager().getNetherPortalsConfig();
                            World netherWorld = Bukkit.getWorld(netherPortalsConfig.getNetherWorld());

                            if (netherWorld == null) {
                                player.sendMessage(MessageManager.msg("unknownDestinationWorld"));
                                this.cancel();
                                return;
                            }

                            RandomSearcher randomSearcher = plugin.getRandomTeleportAPI().getRandomSearcher(player, new Location(netherWorld, 0, 0, 0), netherPortalsConfig.getNetherMinRange(), netherPortalsConfig.getNetherMaxRange());
                            randomSearcher.setMinY(netherPortalsConfig.getNetherMinY());
                            randomSearcher.setMaxY(netherPortalsConfig.getNetherMaxY());

                            randomSearcher.search().thenApply(location -> {
                                location.getChunk().load();
                                NetherPortalUtil.createPortal(location, axis);

                                NetherPortalInfo createdPortalInfo = NetherPortalUtil.getPortalInfo(location.getBlock());

                                double createdPortalX = createdPortalInfo.getCenter().getX();
                                double createdPortalY = createdPortalInfo.getCenter().getY();
                                double createdPortalZ = createdPortalInfo.getCenter().getZ();

                                plugin.getNetherPortalsManager().savePortalAndDestination(fromPortalServer, fromPortalWorldName, fromPortalX, fromPortalY, fromPortalZ, location.getWorld().getName(), createdPortalX, createdPortalY, createdPortalZ);

                                player.teleport(new Location(createdPortalInfo.getCenter().getWorld(), createdPortalInfo.getCenter().getX(), createdPortalInfo.getCenter().getY(), createdPortalInfo.getCenter().getZ(), playerYaw, playerPitch));
                                plugin.getNetherPortalsManager().grantNetherAdvancements(player);

                                return null;
                            });

                            this.cancel();
                        }
                    }
                }.runTaskTimer(plugin, 1, 1);
            }
        }
    }
}
