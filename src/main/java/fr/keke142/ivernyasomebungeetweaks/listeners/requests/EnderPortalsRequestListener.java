package fr.keke142.ivernyasomebungeetweaks.listeners.requests;

import com.gmail.tracebachi.DeltaRedis.Spigot.DeltaRedisMessageEvent;
import fr.keke142.ivernyasomebungeetweaks.IvernyaSomeBungeeTweaksPlugin;
import fr.keke142.ivernyasomebungeetweaks.configs.EnderPortalsConfig;
import fr.keke142.ivernyasomebungeetweaks.managers.MessageManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.UUID;

import static com.gmail.tracebachi.DeltaRedis.Shared.SplitPatterns.DELTA;
import static fr.keke142.ivernyasomebungeetweaks.IvernyaSomeBungeeTweaksPlugin.MAX_PLAYER_WAITING_MS;

public class EnderPortalsRequestListener implements Listener {
    private IvernyaSomeBungeeTweaksPlugin plugin;

    public EnderPortalsRequestListener(IvernyaSomeBungeeTweaksPlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onTeleportToEnderPortalRequest(DeltaRedisMessageEvent event) {
        String channel = event.getChannel();
        String eventMessage = event.getMessage();

        String[] splitMessage = DELTA.split(eventMessage);

        EnderPortalsConfig enderPortalsConfig = plugin.getConfigManager().getEnderPortalsConfig();

        if (channel.equals("enderPortalsRequest")) {
            String playerUUID = splitMessage[0];

            long startTime = System.currentTimeMillis();

            new BukkitRunnable() {
                @Override
                public void run() {
                    long elapsedTime = (System.currentTimeMillis() - startTime);

                    if (elapsedTime < MAX_PLAYER_WAITING_MS) {
                        this.cancel();
                    }

                    if (Bukkit.getPlayer(UUID.fromString(playerUUID)) != null) {
                        Player player = Bukkit.getPlayer(UUID.fromString(playerUUID));

                        World endPortalWorld = Bukkit.getWorld(enderPortalsConfig.getEnderWorld());
                        if (endPortalWorld == null) {
                            player.sendMessage(MessageManager.msg("unknownDestinationWorld"));
                            this.cancel();
                            return;
                        }

                        Location portalLocation = new Location(endPortalWorld, 100, 50, 0);
                        Block portalLocationBlock = portalLocation.getBlock();

                        for (int x = portalLocationBlock.getX() - 2; x <= portalLocationBlock.getX() + 2; x++) {
                            for (int z = portalLocationBlock.getZ() - 2; z <= portalLocationBlock.getZ() + 2; z++) {
                                Block platformBlock = portalLocation.getWorld().getBlockAt(x, portalLocationBlock.getY() - 1, z);
                                if (platformBlock.getType() != Material.OBSIDIAN) {
                                    platformBlock.setType(Material.OBSIDIAN);
                                }
                                for (int yMod = 1; yMod <= 3; yMod++) {
                                    Block b = platformBlock.getRelative(BlockFace.UP, yMod);
                                    if (b.getType() != Material.AIR) {
                                        b.setType(Material.AIR);
                                    }
                                }
                            }
                        }

                        player.teleport(portalLocation);
                        plugin.getEnderPortalsManager().grantEndAdvancements(player);

                        this.cancel();
                    }
                }
            }.runTaskTimer(plugin, 1, 1);
        }
    }
}
