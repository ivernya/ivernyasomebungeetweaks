package fr.keke142.ivernyasomebungeetweaks.listeners;

import com.gmail.tracebachi.DeltaRedis.Spigot.DeltaRedisApi;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import fr.keke142.ivernyasomebungeetweaks.IvernyaSomeBungeeTweaksPlugin;
import fr.keke142.ivernyasomebungeetweaks.configs.EnderPortalsConfig;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityCombustEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerPortalEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public class EnderPortalsListener implements Listener {
    private IvernyaSomeBungeeTweaksPlugin plugin;
    private List<String> enteredPortal;

    public EnderPortalsListener(IvernyaSomeBungeeTweaksPlugin plugin) {
        this.plugin = plugin;

        enteredPortal = new ArrayList<>();
    }

    @EventHandler
    public void onPlayerDamage(EntityDamageEvent e) {
        if (!(e.getEntity() instanceof Player)) return;

        Player p = (Player) e.getEntity();

        if (!enteredPortal.contains(p.getUniqueId().toString())) return;

        if (e.getCause() == EntityDamageEvent.DamageCause.LAVA || e.getCause() == EntityDamageEvent.DamageCause.FIRE || e.getCause() == EntityDamageEvent.DamageCause.FIRE_TICK) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onEntityCombust(EntityCombustEvent e) {
        if (!(e.getEntity() instanceof Player)) return;

        Player p = (Player) e.getEntity();

        if (!enteredPortal.contains(p.getUniqueId().toString())) return;

        e.setCancelled(true);
    }

    @EventHandler
    public void playerPortalEvent(PlayerPortalEvent e) {
        if (e.getCause() != PlayerTeleportEvent.TeleportCause.END_PORTAL) return;

        e.setCancelled(true);

        Player player = e.getPlayer();

        String playerUuid = player.getUniqueId().toString();

        if (enteredPortal.contains(playerUuid)) return;

        enteredPortal.add(playerUuid);
        new BukkitRunnable() {
            @Override
            public void run() {
                enteredPortal.remove(playerUuid);
            }
        }.runTaskLater(plugin, 100);

        DeltaRedisApi deltaApi = DeltaRedisApi.instance();

        String currentServer = deltaApi.getServerName();

        EnderPortalsConfig enderPortalsConfig = plugin.getConfigManager().getEnderPortalsConfig();

        String enderServer = enderPortalsConfig.getEnderServer();

        if (currentServer.equalsIgnoreCase(enderServer)) {
            //TODO for the moment masuite handle spawn teleport after entering the portal
        } else {
            ByteArrayDataOutput out = ByteStreams.newDataOutput();
            out.writeUTF("Connect");
            out.writeUTF(enderServer);

            player.sendPluginMessage(plugin, "BungeeCord", out.toByteArray());

            deltaApi.publish(enderServer, "enderPortalsRequest", playerUuid);
        }

    }
}
