package fr.keke142.ivernyasomebungeetweaks.listeners;

import com.gmail.tracebachi.DeltaRedis.Spigot.DeltaRedisApi;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import fr.keke142.ivernyasomebungeetweaks.IvernyaSomeBungeeTweaksPlugin;
import fr.keke142.ivernyasomebungeetweaks.NetherPortalUtil;
import fr.keke142.ivernyasomebungeetweaks.managers.MessageManager;
import fr.keke142.ivernyasomebungeetweaks.objects.NetherPortal;
import fr.keke142.ivernyasomebungeetweaks.objects.NetherPortalInfo;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPortalEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.scheduler.BukkitRunnable;

public class NetherPortalsListener implements Listener {
    private IvernyaSomeBungeeTweaksPlugin plugin;

    public NetherPortalsListener(IvernyaSomeBungeeTweaksPlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void playerPortalEvent(PlayerPortalEvent e) {
        if (e.getCause() != PlayerTeleportEvent.TeleportCause.NETHER_PORTAL) return;

        e.setCancelled(true);

        Player player = e.getPlayer();

        long startTime = System.currentTimeMillis();

        new BukkitRunnable() {
            @Override
            public void run() {
                long elapsedTimeInSecs = (System.currentTimeMillis() - startTime) / 1000;

                if (player.getLocation().getBlock().getType() != Material.NETHER_PORTAL) {
                    this.cancel();
                }

                if (elapsedTimeInSecs >= 4) {
                    this.cancel();

                    NetherPortalInfo netherPortalInfo = NetherPortalUtil.getPortalInfo(e.getFrom().getBlock());

                    DeltaRedisApi deltaApi = DeltaRedisApi.instance();

                    String portalServer = deltaApi.getServerName();
                    String portalWorld = player.getWorld().getName();

                    double portalCenterX = netherPortalInfo.getCenter().getX();
                    double portalCenterY = netherPortalInfo.getCenter().getY();
                    double portalCenterZ = netherPortalInfo.getCenter().getZ();

                    String portalCenterXS = String.valueOf(portalCenterX);
                    String portalCenterYS = String.valueOf(portalCenterY);
                    String portalCenterZS = String.valueOf(portalCenterZ);

                    String playerYawS = String.valueOf(player.getLocation().getYaw());
                    String playerPitchS = String.valueOf(player.getLocation().getPitch());

                    String netherServer = plugin.getConfigManager().getNetherPortalsConfig().getNetherServer();

                    if (portalServer.equalsIgnoreCase(netherServer)) {
                        try {
                            NetherPortal portal = plugin.getDatabaseManager().getNetherPortals().getPortalFromFromTo(portalWorld, portalCenterX, portalCenterY, portalCenterZ);

                            if (portal == null) {
                                player.sendMessage(MessageManager.msg("netherportals.noDestination"));
                                return;
                            }

                            ByteArrayDataOutput out = ByteStreams.newDataOutput();
                            out.writeUTF("Connect");
                            out.writeUTF(portal.getServer());

                            player.sendPluginMessage(plugin, "BungeeCord", out.toByteArray());

                            deltaApi.publish(portal.getServer(), "teleportToOverworldPortalRequest", portal.getWorld(), String.valueOf(portal.getX()), String.valueOf(portal.getY()), String.valueOf(portal.getZ()), player.getUniqueId().toString(), playerYawS, playerPitchS);
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                    } else {

                        String isXAxis = Boolean.toString((netherPortalInfo.getAxis() == NetherPortalInfo.Axis.x));

                        ByteArrayDataOutput out = ByteStreams.newDataOutput();
                        out.writeUTF("Connect");
                        out.writeUTF(netherServer);

                        player.sendPluginMessage(plugin, "BungeeCord", out.toByteArray());

                        deltaApi.publish(netherServer, "netherPortalsRequest", portalServer, portalWorld, portalCenterXS, portalCenterYS, portalCenterZS, player.getUniqueId().toString(), isXAxis, playerYawS, playerPitchS);
                    }
                }
            }
        }.runTaskTimer(plugin, 10, 10);
    }
}
