package fr.keke142.ivernyasomebungeetweaks.listeners;

import com.gmail.tracebachi.DeltaRedis.Spigot.DeltaRedisMessageEvent;
import fr.keke142.ivernyasomebungeetweaks.IvernyaSomeBungeeTweaksPlugin;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerAdvancementDoneEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;

import static com.gmail.tracebachi.DeltaRedis.Shared.SplitPatterns.DELTA;

public class PlayerDataBridgeListener implements Listener {
    public static final int SAVE_DATA_MS = 6000;
    private IvernyaSomeBungeeTweaksPlugin plugin;

    public PlayerDataBridgeListener(IvernyaSomeBungeeTweaksPlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerQuitEvent(PlayerQuitEvent e) {
        plugin.getPlayerDataBridgeManager().bungeeSaveDataForPlayer(e.getPlayer(), true);
    }

    @EventHandler
    public void onAdvancementDone(PlayerAdvancementDoneEvent e) {

    }

    @EventHandler
    public void onAdvancementsReceived(DeltaRedisMessageEvent e) {
        String channel = e.getChannel();
        String eventMessage = e.getMessage();

        if (channel.equals("sendPlayerDataToServers")) {
            String[] splitMessage = DELTA.split(eventMessage);

            String playerUuid = splitMessage[0];
            String jsonAdvancements = splitMessage[1];
            String jsonStats = splitMessage[2];
            boolean triggerRefresh = Boolean.parseBoolean(splitMessage[3]);

            if (triggerRefresh) {
                plugin.getPlayerDataBridgeManager().bungeeLoadDataForPlayer(playerUuid, jsonAdvancements, jsonStats, true);
            } else {
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        plugin.getPlayerDataBridgeManager().bungeeLoadDataForPlayer(playerUuid, jsonAdvancements, jsonStats, false);
                    }

                }.runTaskAsynchronously(plugin);
            }
        }
    }
}
