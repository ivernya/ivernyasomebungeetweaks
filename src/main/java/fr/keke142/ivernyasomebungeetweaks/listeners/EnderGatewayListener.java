package fr.keke142.ivernyasomebungeetweaks.listeners;

import de.themoep.randomteleport.searcher.RandomSearcher;
import fr.keke142.ivernyasomebungeetweaks.IvernyaSomeBungeeTweaksPlugin;
import fr.keke142.ivernyasomebungeetweaks.configs.EnderGatewaysConfig;
import io.papermc.lib.PaperLib;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.potion.PotionEffectType;

public class EnderGatewayListener implements Listener {
    private IvernyaSomeBungeeTweaksPlugin plugin;

    public EnderGatewayListener(IvernyaSomeBungeeTweaksPlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void playerTeleportEvent(PlayerTeleportEvent e) {
        if (e.getCause() != PlayerTeleportEvent.TeleportCause.END_GATEWAY) return;

        e.setCancelled(true);

        Player player = e.getPlayer();

        EnderGatewaysConfig gatewaysConfig = plugin.getConfigManager().getEnderGatewaysConfig();

        RandomSearcher searcher = plugin.getRandomTeleportAPI().getRandomSearcher(player, new Location(player.getWorld(), 0, 0, 0), gatewaysConfig.getGatewaysMinRange(), gatewaysConfig.getGatewaysMaxRange());
        searcher.search().thenApply(targetLoc -> {
            searcher.getTargets().forEach(p -> {
                Location belowLoc = targetLoc.clone().subtract(0, 1, 0);
                Block belowBlock = belowLoc.getBlock();
                ((Player) p).sendBlockChange(belowLoc, belowBlock.getType(), belowBlock.getData());

                targetLoc.setX(targetLoc.getBlockX() + 0.5);
                targetLoc.setY(targetLoc.getY() + 0.1);
                targetLoc.setZ(targetLoc.getBlockZ() + 0.5);

                PaperLib.teleportAsync(p, targetLoc);
                player.removePotionEffect(PotionEffectType.BLINDNESS);
            });
            return true;
        });
    }
}
